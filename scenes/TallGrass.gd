extends Area2D

export(int, 0, 100, 1) var event_chance_precent = 40


func _on_TallGrass_body_entered(body):
	if global.just_finished:
		return

	if body.is_in_group('player'):
		if randi() % 100 < event_chance_precent:
			# print("EVENT!!")
			
			var return_value = get_tree().change_scene("res://scenes/Battle.tscn")
			if return_value == OK:
				global.player_position = body.position
