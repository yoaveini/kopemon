extends TileMap


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	global.bed_position = $Bed.position
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Bed_heal():
	$Player.in_dialog = true
	$HealMessage.visible = true
	
	$HealMessageTimer.start()
	yield($HealMessageTimer, "timeout")
	
	$HealMessage.visible = false
	$Player.in_dialog = false
