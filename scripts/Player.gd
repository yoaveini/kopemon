extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var direction = Vector2.ZERO
export var speed = 200
var in_dialog = false
var last_direction = 'down'
var mercy_timer_started = false

# Called when the node enters the scene tree for the first time.
func _ready():
	position = global.player_position
	pass # Replace with function body.


func update_direction():
	direction = Vector2.ZERO
	
	if in_dialog:
		return
	
	if Input.is_action_pressed("right"):
		direction.x = 1
		last_direction = 'right'
	elif Input.is_action_pressed("left"):
		direction.x = -1
		last_direction = 'left'
	elif Input.is_action_pressed("up"):
		direction.y = -1
		last_direction = 'up'
	elif Input.is_action_pressed("down"):
		direction.y = 1
		last_direction = 'down'
		
	if !mercy_timer_started and direction != Vector2.ZERO:
		$MercyTimer.start()
		mercy_timer_started = true
		
func set_animation():
	if direction == Vector2.ZERO:
		$AnimatedSprite.play("idle_" + last_direction)
	else:
		$AnimatedSprite.play("walk_" + last_direction)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var _delta = delta # unused
	
	update_direction()
	set_animation()
	
	var _return_value = move_and_slide(direction * speed) # unused


func _on_MercyTimer_timeout():
	global.just_finished = false
	mercy_timer_started = false
