extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var enemy: global.Kopemon


func generate_enemy():
	var level = (randi() % global.player.get_level()) + 4
	return global.Kopemon.new(level)
	

func update_text(message: String):
	$Player.text = "Player:\n" + str(global.player)
	$Enemy.text = "Enemy:\n" + str(enemy)
	$Message.visible = true
	
	$Message.text = message
	
	$MessageTimer.start()
	# yield($MessageTimer, "timeout")

# Called when the node enters the scene tree for the first time.
func _ready():
	$Attack.disabled = true
	enemy = generate_enemy()
	update_text("A wild Enemy appeared!")
	yield($MessageTimer, "timeout")
	$Attack.disabled = false


func go_back_to_map():
	var return_value = get_tree().change_scene("res://scenes/Map.tscn")
	if return_value == OK:
		global.just_finished = true


func _on_Attack_pressed():
	$Attack.disabled = true
	
	global.player.attack(enemy)
	# print(global.player._damage)
	update_text("Player has attacked Enemy")
	
	yield($MessageTimer, "timeout")
	
	
	if enemy.is_fainted():
		var xp = enemy._level * enemy._level * 2
		var levels_gained = global.player.gain_xp(xp)
		
		update_text("Enemy has fainted")
		yield($MessageTimer, "timeout")
		update_text("Player Received: {xp} xp".format({"xp": xp}))
		yield($MessageTimer, "timeout")
		
		if levels_gained > 0:
			update_text("Player has leveled up to level: {level}".format({"level": global.player.get_level()}))
			yield($MessageTimer, "timeout")
		
		go_back_to_map()

	else:
		enemy.attack(global.player)	
		
		update_text("Enemy has attacked Player")
		yield($MessageTimer, "timeout")
		
		if global.player.is_fainted():
			global.player_position = global.bed_position
			update_text("Player has fainted")
			yield($MessageTimer, "timeout")
			
			go_back_to_map()
		
	$Attack.disabled = false


func _on_MessageTimer_timeout():
	$Message.visible = false
	pass # Replace with function body.
