extends Node

var player_position = Vector2(0, 0)
var bed_position: Vector2
var just_finished = false

class Kopemon:
	var _max_hp
	var _hp
	var _damage
	var _level
	var _xp
	var _next_level_xp
	
	func _init(level: int, max_hp: int = 4, damage: int = 2):
		randomize()
		_max_hp = max_hp
		_hp = _max_hp
		_damage = damage
		_level = 1
		_xp = 0
		_next_level_xp = _level * _level * 3
		
		_level_up(level - 1)
	
	func attack(target: Kopemon):
		target._hp -= _damage
		if target._hp < 0:
			target._hp = 0
		
	func gain_xp(xp):
		_xp += xp
		
		var levels = 0
		
		while _xp >= _next_level_xp:
			_level_up()
			levels += 1
			
		return levels
		
	func heal(hp = -1):
		if hp == -1:
			_hp = _max_hp
			return
		
		_hp += hp
		if _hp > _max_hp:
			_hp = _max_hp
		
			
	func get_level():
		return _level
			
	func _level_up(levels: int = 1):
		for _i in range(levels):
			_level += 1
			
			_xp = _xp % _next_level_xp
			_next_level_xp = _level * _level * 2
			
			_damage += randi() % 3 + 1
			var hp_boost = randi() % 6 + 1
			
			_max_hp += hp_boost
			_hp += hp_boost

	func is_fainted() -> bool:
		return _hp <= 0
		
	func _to_string():
		var format = "hp: {hp}/{max_hp}\n"
		format += "damage: {damage}\n"
		format += "level: {level}\n"
		format += "xp: {xp}/{next_level_xp}"
		
		return format.format({'hp': _hp, "max_hp": _max_hp, "damage": _damage,
		"level": _level, "xp": _xp, "next_level_xp": _next_level_xp})

var player = Kopemon.new(5, 10, 4)
